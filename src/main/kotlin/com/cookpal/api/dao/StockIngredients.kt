package com.cookpal.api.dao

import org.jetbrains.exposed.sql.Table


object StockIngredients: Table() {
    val id = integer("id").autoIncrement()
    val userId = integer("user_id") references Users.id
    val ingredientId = integer("ingredient_id") references Ingredients.id
    val measure = varchar("measure", 50)
    val quantity = integer("quantity")
    override val primaryKey = PrimaryKey(id)
}