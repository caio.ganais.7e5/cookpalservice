package com.cookpal.api.dao

import com.cookpal.api.models.Category
import org.jetbrains.exposed.sql.Table

object Ingredients: Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 50)
    val category = enumeration<Category>("category")
    val img = varchar("image", 1000).nullable()

    override val primaryKey = PrimaryKey(id)
}