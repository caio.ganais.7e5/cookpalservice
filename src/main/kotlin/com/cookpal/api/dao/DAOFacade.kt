package com.cookpal.api.dao

import com.cookpal.api.models.*

interface DAOFacade {

    suspend fun allUsers(): List<User>
    suspend fun addUser(username: String, email: String, password: String): User?
    suspend fun editUser(id: Int, username: String, email: String, password: String, recipes: List<Recipe>, stock: List<IngredientContainer>, shop: List<IngredientContainer>): Boolean
    suspend fun deleteUser(id: Int): Boolean
    suspend fun getUserById(id: Int): User?
    suspend fun userExists(id: Int): Boolean
    suspend fun getUserByUsername(username: String): User?
    suspend fun getRecipesByUserId(userId: Int): List<Recipe>
    suspend fun getStockIngredientsByUserId(userId: Int): List<IngredientContainer>
    suspend fun getShopIngredientsByUserId(userId: Int): List<IngredientContainer>
    suspend fun addRecipeToUser(userId: Int, recipeId: Int): Boolean
    suspend fun addIngredientToUserStock(userId: Int, ingredientContainer: IngredientContainer): Boolean
    suspend fun addIngredientToUserShop(userId: Int, ingredientContainer: IngredientContainer): Boolean
    suspend fun deleteRecipeFromUser(userId: Int, recipeId: Int): Boolean
    suspend fun deleteIngredientFromUserStock(userId: Int, ingredientId: Int): Boolean
    suspend fun deleteIngredientFromUserShop(userId: Int, ingredientId: Int): Boolean



    suspend fun allIngredients(): List<Ingredient>
    suspend fun getIngredientById(id: Int): Ingredient?
    suspend fun getIngredientByName(name: String): Ingredient?
    suspend fun addNewIngredient(name: String, category: Category, img: String?): Ingredient?
    suspend fun editIngredient(id: Int, name: String, category: Category, img: String?): Boolean
    suspend fun deleteIngredient(id: Int): Boolean


    suspend fun allRecipes(): List<Recipe>
    suspend fun getRecipeById(id: Int): Recipe?
    suspend fun addNewRecipe(name: String, dinnerCommensal: Int, cookTime: Int, ingredients: List<IngredientContainer>, steps: List<String>): Recipe?
    suspend fun editRecipe(id: Int, name: String, dinnerCommensal: Int, cookTime: Int, ingredients: List<IngredientContainer>, steps: List<String> ): Boolean
    suspend fun deleteRecipe(id: Int): Boolean
    suspend fun getRecipeIngredients(recipeId: Int): List<IngredientContainer>
    suspend fun addIngredientToRecipe(recipeId: Int, ingredientContainer: IngredientContainer): Boolean
}


