package com.cookpal.api.dao

import org.jetbrains.exposed.sql.Table


object RecipeIngredients: Table() {
    val id = integer("id").autoIncrement()
    val recipeId = integer("recipe_id") references Recipes.id
    val ingredientId = integer("ingredient_id") references Ingredients.id
    val measure = varchar("measure", 50)
    val quantity = integer("quantity")
    override val primaryKey = PrimaryKey(id)
}
