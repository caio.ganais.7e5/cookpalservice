package com.cookpal.api.dao

import com.cookpal.api.dao.DatabaseFactory.dbQuery
import com.cookpal.api.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOFacadeImpl : DAOFacade{

    private fun resultRowToIngredient(row: ResultRow): Ingredient = Ingredient(
        ingredientId = row[Ingredients.id],
        name = row[Ingredients.name],
        category = row[Ingredients.category],
        img = row[Ingredients.img]
    )


    private suspend fun resultRowToIngredientContainer(row: ResultRow): IngredientContainer = IngredientContainer(
        ingredient = getIngredientById(row[RecipeIngredients.ingredientId])!!,
        quantity = row[RecipeIngredients.quantity],
        measure = row[RecipeIngredients.measure]
    )





    override suspend fun allUsers(): List<User> = dbQuery {
        Users
            .selectAll()
            .map{row ->
                User(
                    userId = row[Users.id],
                    username = row[Users.username],
                    email = row[Users.email],
                    password = row[Users.password],
                    recipes = getRecipesByUserId(row[Users.id]),
                    stockIngredients = getStockIngredientsByUserId(row[Users.id]),
                    shopIngredients = getShopIngredientsByUserId(row[Users.id])
                )
            }
    }

    override suspend fun addUser(username: String, email: String, password: String): User? = dbQuery {
        Users.insert{
            it[Users.username] = username
            it[Users.email] = email
            it[Users.password] = password
        }.resultedValues?.get(0)?.let { row ->
            User(
                userId = row[Users.id],
                username = row[Users.username],
                email = row[Users.email],
                password = row[Users.password],
                recipes = getRecipesByUserId(row[Users.id]),
                stockIngredients = getStockIngredientsByUserId(row[Users.id]),
                shopIngredients = getShopIngredientsByUserId(row[Users.id])
            )
        }

    }

    override suspend fun editUser(id: Int,
                                  username: String,
                                  email: String,
                                  password: String,
                                  recipes: List<Recipe>,
                                  stock: List<IngredientContainer>,
                                  shop: List<IngredientContainer>): Boolean = dbQuery {
        Users.update({Users.id eq id}){
            it[Users.username] = username
            it[Users.email] = email
            it[Users.password] = password
        }
        UserRecipes.deleteWhere { UserRecipes.userId eq id }
        StockIngredients.deleteWhere { StockIngredients.userId eq id }
        ShopIngredients.deleteWhere { ShopIngredients.userId eq id }
        recipes.forEach { recipe ->
            UserRecipes.insert {
                it[UserRecipes.userId] = id
                it[UserRecipes.recipeId] = recipe.recipeId
            }
        }
        stock.forEach { ingredientContainer ->
            StockIngredients.insert {
                it[StockIngredients.userId] = id
                it[StockIngredients.ingredientId] = ingredientContainer.ingredient.ingredientId
                it[StockIngredients.quantity] = ingredientContainer.quantity
                it[StockIngredients.measure] = ingredientContainer.measure
            }
        }
        shop.forEach { ingredientContainer ->
            ShopIngredients.insert {
                it[ShopIngredients.userId] = id
                it[ShopIngredients.ingredientId] = ingredientContainer.ingredient.ingredientId
                it[ShopIngredients.quantity] = ingredientContainer.quantity
                it[ShopIngredients.measure] = ingredientContainer.measure
            }
        }
        true
    }

    override suspend fun deleteUser(id: Int): Boolean = dbQuery {
        UserRecipes.deleteWhere { UserRecipes.userId eq id }
        StockIngredients.deleteWhere { StockIngredients.userId eq id }
        ShopIngredients.deleteWhere { ShopIngredients.userId eq id }
        Users.deleteWhere { Users.id eq id } > 0
    }

    override suspend fun getUserById(id: Int): User? = dbQuery {
        Users
            .select { Users.id eq id }
            .map { row ->
                User(
                    userId = row[Users.id],
                    username = row[Users.username],
                    email = row[Users.email],
                    password = row[Users.password],
                    recipes = getRecipesByUserId(row[Users.id]),
                    stockIngredients = getStockIngredientsByUserId(row[Users.id]),
                    shopIngredients = getShopIngredientsByUserId(row[Users.id])
                )
            }
            .singleOrNull()
    }

    override suspend fun userExists(id: Int): Boolean = dbQuery {
        Users.select { Users.id eq id }.count() > 0
    }

    override suspend fun getUserByUsername(username: String): User? = dbQuery {
        Users
            .select { Users.username eq username }
            .map { row ->
                User(
                    userId = row[Users.id],
                    username = row[Users.username],
                    email = row[Users.email],
                    password = row[Users.password],
                    recipes = getRecipesByUserId(row[Users.id]),
                    stockIngredients = getStockIngredientsByUserId(row[Users.id]),
                    shopIngredients = getShopIngredientsByUserId(row[Users.id])
                )
            }
            .singleOrNull()
    }

    override suspend fun getRecipesByUserId(userId: Int): List<Recipe> = dbQuery {
        UserRecipes
            .select { UserRecipes.userId eq userId }
            .map {row ->
                getRecipeById(row[UserRecipes.recipeId])!!
            }
    }

    override suspend fun getStockIngredientsByUserId(userId: Int): List<IngredientContainer> = dbQuery{
        StockIngredients
            .select { StockIngredients.userId eq userId }
            .map { row -> getIngredientById(row[StockIngredients.ingredientId])?.let { ingredient ->
                IngredientContainer(
                    ingredient = ingredient,
                    quantity = row[StockIngredients.quantity],
                    measure = row[StockIngredients.measure]
                )
            }!! }

    }

    override suspend fun getShopIngredientsByUserId(userId: Int): List<IngredientContainer> = dbQuery{
        ShopIngredients
            .select { ShopIngredients.userId eq userId }
            .map { row -> getIngredientById(row[ShopIngredients.ingredientId])?.let { ingredient ->
                IngredientContainer(
                    ingredient = ingredient,
                    quantity = row[ShopIngredients.quantity],
                    measure = row[ShopIngredients.measure]
                )
            }!! }
    }

    override suspend fun addRecipeToUser(userId: Int, recipeId: Int): Boolean = dbQuery {
        UserRecipes.insert {
            it[UserRecipes.userId] = userId
            it[UserRecipes.recipeId] = recipeId
        }
        true
    }

    override suspend fun addIngredientToUserStock(userId: Int, ingredientContainer: IngredientContainer): Boolean = dbQuery {
        StockIngredients.insert {
            it[StockIngredients.userId] = userId
            it[StockIngredients.ingredientId] = ingredientContainer.ingredient.ingredientId
            it[StockIngredients.quantity] = ingredientContainer.quantity
            it[StockIngredients.measure] = ingredientContainer.measure
        }
        true
    }

    override suspend fun addIngredientToUserShop(userId: Int, ingredientContainer: IngredientContainer): Boolean = dbQuery {
        ShopIngredients.insert {
            it[ShopIngredients.userId] = userId
            it[ShopIngredients.ingredientId] = ingredientContainer.ingredient.ingredientId
            it[ShopIngredients.quantity] = ingredientContainer.quantity
            it[ShopIngredients.measure] = ingredientContainer.measure
        }
        true
    }

    override suspend fun deleteRecipeFromUser(userId: Int, recipeId: Int): Boolean = dbQuery{
        UserRecipes.deleteWhere {
            (UserRecipes.userId eq userId) and (UserRecipes.recipeId eq recipeId)
        }
        true
    }

    override suspend fun deleteIngredientFromUserStock(userId: Int, ingredientId: Int): Boolean =  dbQuery{
        StockIngredients.deleteWhere {
            (StockIngredients.userId eq userId) and (StockIngredients.ingredientId eq ingredientId)
        }
        true
    }

    override suspend fun deleteIngredientFromUserShop(userId: Int, ingredientId: Int): Boolean = dbQuery {
        ShopIngredients.deleteWhere {
            (ShopIngredients.userId eq userId) and (ShopIngredients.ingredientId eq ingredientId)
        }
        true
    }

    override suspend fun allIngredients(): List<Ingredient> = dbQuery {
        Ingredients.selectAll().map(::resultRowToIngredient)
    }


    override suspend fun getIngredientById(id: Int): Ingredient? = dbQuery {
        Ingredients
            .select { Ingredients.id eq id }
            .map(::resultRowToIngredient)
            .singleOrNull()
    }


    override suspend fun getIngredientByName(name: String): Ingredient? = dbQuery {
        Ingredients
            .select { Ingredients.name eq name }
            .map(::resultRowToIngredient)
            .singleOrNull()
    }


    override suspend fun addNewIngredient(name: String, category: Category, img: String?): Ingredient? = dbQuery {
            val insertStatement = Ingredients.insert {
                it[Ingredients.name] = name
                it[Ingredients.category] = category
                it[Ingredients.img] = img
            }
            insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToIngredient)
    }


    override suspend fun editIngredient(id: Int, name: String, category: Category, img: String?): Boolean = dbQuery {
        Ingredients.update({ Ingredients.id eq id }) {
            it[Ingredients.name] = name
            it[Ingredients.category] = category
            it[Ingredients.img] = img
        } > 0
    }


    override suspend fun deleteIngredient(id: Int): Boolean = dbQuery {
        Ingredients.deleteWhere { Ingredients.id eq id } > 0
    }


    override suspend fun allRecipes(): List<Recipe> = dbQuery {
        Recipes
            .selectAll()
            .map{row ->
                Recipe(
                    recipeId = row[Recipes.id],
                    name = row[Recipes.name],
                    ingredients = getRecipeIngredients(row[Recipes.id]),
                    dinnerCommensal = row[Recipes.dinnerCommensal],
                    cookTime = row[Recipes.cookTime],
                    steps = row[Recipes.steps].split("\n")
                )
            }
    }

    override suspend fun getRecipeById(id: Int): Recipe? = dbQuery {
        Recipes
            .select { Recipes.id eq id }
            .map{row ->
                Recipe(
                    recipeId = row[Recipes.id],
                    name = row[Recipes.name],
                    ingredients = getRecipeIngredients(row[Recipes.id]),
                    dinnerCommensal = row[Recipes.dinnerCommensal],
                    cookTime = row[Recipes.cookTime],
                    steps = row[Recipes.steps].split("\n")
                )
            }
            .singleOrNull()
    }

    override suspend fun addNewRecipe(
        name: String,
        dinnerCommensal: Int,
        cookTime: Int,
        ingredients: List<IngredientContainer>,
        steps: List<String>
    ): Recipe? = dbQuery {
        val insertStatement = Recipes.insert {
            it[Recipes.name] = name
            it[Recipes.dinnerCommensal] = dinnerCommensal
            it[Recipes.cookTime] = cookTime
            it[Recipes.steps] = steps.joinToString("\n")
        }
        insertStatement.resultedValues?.singleOrNull()?.let { row ->
            val recipeId = row[Recipes.id]
            ingredients.forEach { ingredientContainer ->
                val ingredient = getIngredientByName(ingredientContainer.ingredient.name)
                if (ingredient != null) {
                    RecipeIngredients.insert {
                        it[RecipeIngredients.recipeId] = recipeId
                        it[RecipeIngredients.ingredientId] = ingredient.ingredientId
                        it[RecipeIngredients.quantity] = ingredientContainer.quantity
                        it[RecipeIngredients.measure] = ingredientContainer.measure
                    }
                }
            }
            Recipe(
                recipeId = recipeId,
                name = name,
                dinnerCommensal = dinnerCommensal,
                cookTime = cookTime,
                ingredients = ingredients,
                steps = steps
            )
        }
    }

    override suspend fun editRecipe(
        id: Int,
        name: String,
        dinnerCommensal: Int,
        cookTime: Int,
        ingredients: List<IngredientContainer>,
        steps: List<String>
    ): Boolean = dbQuery {

        Recipes.update({ Recipes.id eq id }) {
            it[Recipes.name] = name
            it[Recipes.dinnerCommensal] = dinnerCommensal
            it[Recipes.cookTime] = cookTime
            it[Recipes.steps] = steps.joinToString("\n")
        } > 0

        RecipeIngredients.deleteWhere { RecipeIngredients.recipeId eq id }
            ingredients.forEach { ingredientContainer ->
                val ingredient = getIngredientByName(ingredientContainer.ingredient.name)
                if (ingredient != null) {
                    RecipeIngredients.insert {
                        it[RecipeIngredients.recipeId] = id
                        it[RecipeIngredients.ingredientId] = ingredient.ingredientId
                        it[RecipeIngredients.quantity] = ingredientContainer.quantity
                        it[RecipeIngredients.measure] = ingredientContainer.measure
                    }
                }
        }
        true

    }

    override suspend fun deleteRecipe(id: Int): Boolean  = dbQuery {
        RecipeIngredients.deleteWhere { RecipeIngredients.recipeId eq id }
        Recipes.deleteWhere { Recipes.id eq id } > 0
    }

    override suspend fun getRecipeIngredients(recipeId: Int): List<IngredientContainer> = dbQuery {
        RecipeIngredients
            .select { RecipeIngredients.recipeId eq recipeId }
            .map { row -> resultRowToIngredientContainer(row) }
    }

    override suspend fun addIngredientToRecipe(recipeId: Int, ingredientContainer: IngredientContainer): Boolean = dbQuery{
        val ingredient = getIngredientByName(ingredientContainer.ingredient.name)
        if (ingredient != null) {
            RecipeIngredients.insert {
                it[RecipeIngredients.recipeId] = recipeId
                it[RecipeIngredients.ingredientId] = ingredient.ingredientId
                it[RecipeIngredients.quantity] = ingredientContainer.quantity
                it[RecipeIngredients.measure] = ingredientContainer.measure
            }
        }
        true
    }


}