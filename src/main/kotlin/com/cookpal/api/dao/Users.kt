package com.cookpal.api.dao

import org.jetbrains.exposed.sql.Table

object Users : Table() {

    val id = integer("id").autoIncrement()
    val username = varchar("username", length = 50).uniqueIndex()
    val email = varchar("email", length = 255).uniqueIndex()
    val password = varchar("password", length = 255)

    override val primaryKey = PrimaryKey(id)
}