package com.cookpal.api.dao

import org.jetbrains.exposed.sql.Table

object Recipes: Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 50)
    val dinnerCommensal = integer("dinner_commensal")
    val cookTime = integer("cook_time")
    val steps = varchar("steps", 2000)

    override val primaryKey = PrimaryKey(id)
}
