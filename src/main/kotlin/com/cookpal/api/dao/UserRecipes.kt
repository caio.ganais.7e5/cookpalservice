package com.cookpal.api.dao

import org.jetbrains.exposed.sql.Table

object UserRecipes: Table() {
    val id = integer("id").autoIncrement()
    val userId = integer("user_id") references Users.id
    val recipeId = integer("recipe_id") references Recipes.id
    override val primaryKey = PrimaryKey(id)
}