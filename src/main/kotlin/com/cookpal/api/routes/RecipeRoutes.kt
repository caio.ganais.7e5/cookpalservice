package com.cookpal.api.routes

import com.cookpal.api.dao.DAOFacade
import com.cookpal.api.models.IngredientContainer
import com.cookpal.api.models.Recipe
import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.resources.post
import io.ktor.server.resources.put
import io.ktor.server.response.*
import io.ktor.server.routing.*


@Resource("/recipes")
class Recipes() {
    @Resource("{id}")
    class Id(val parent: Recipes = Recipes(), val id: Int)
}

fun Route.recipesRoutes(dao: DAOFacade){
    /**
     *  Handle the GET request for all recipes.
     */
    get<Recipes> {
        val recipes = dao.allRecipes()
        call.respond(recipes)
    }

    /**
     * Handle the GET request for a specific recipe.
     */
    get<Recipes.Id> {
        val recipe = dao.getRecipeById(it.id)
        if (recipe != null) {
            call.respond(recipe)
        } else {
            call.respondText("La receta con id ${it.id} no existe.", status = HttpStatusCode.NotFound)
        }

    }

    /**
     * Handle the POST request for creating a new recipe.
     */
    post<Recipes>{
        val formParameters = call.receive<Recipe>()
        val recipe = dao.addNewRecipe(formParameters.name,
            formParameters.dinnerCommensal,
            formParameters.cookTime,
            formParameters.ingredients,
            formParameters.steps)

        if (recipe != null) {
            call.respond(recipe)
        }
        else {
            call.respondText("No se pudo crear la receta.", status = HttpStatusCode.InternalServerError)
        }
    }

    /**
     * Handle the PUT request for updating a recipe.
     */
    put<Recipes.Id>{
        val formParameters = call.receive<Recipe>()
        val recipe = dao.editRecipe(it.id,
            formParameters.name,
            formParameters.dinnerCommensal,
            formParameters.cookTime,
            formParameters.ingredients,
            formParameters.steps)

        if (recipe != null) {
            call.respond(recipe)
        }
        else {
            call.respondText("No se pudo actualizar la receta.", status = HttpStatusCode.InternalServerError)
        }
    }

    /**
     * Handle the DELETE request for deleting a recipe.
     */
    delete<Recipes.Id>{
        val recipe = dao.deleteRecipe(it.id)
        if (recipe != null) {
            call.respond(recipe)
        }
        else {
            call.respondText("No se pudo eliminar la receta.", status = HttpStatusCode.InternalServerError)
        }
    }

    /**
     * Handle the POST request for adding an ingredient to a recipe.
     */
    post<Recipes.Id>{
       val formParameters = call.receive<IngredientContainer>()
         val recipe = dao.addIngredientToRecipe(
             it.id,
             formParameters
         )
        if (recipe){
            call.respondText("Se ha agregado el ingrediente a la receta.", status = HttpStatusCode.OK)
        }
        else {
            call.respondText("No se pudo agregar el ingrediente a la receta.", status = HttpStatusCode.InternalServerError)
        }
    }

}
