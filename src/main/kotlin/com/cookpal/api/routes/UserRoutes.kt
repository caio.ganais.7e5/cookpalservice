package com.cookpal.api.routes

import com.cookpal.api.dao.DAOFacade
import com.cookpal.api.models.IngredientContainer
import com.cookpal.api.models.Recipe
import com.cookpal.api.models.User
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.resources.post
import io.ktor.server.resources.put
import io.ktor.server.response.*
import io.ktor.server.routing.*

@Resource("/login")
class Login() {
    @Resource("{id}")
    class Id(val parent: Users = Users(), val id: Int)
    @Resource("{name}")
    class Name(val parent : Users = Users(), val name: String)
}

@Resource("/users")
class Users() {
    @Resource("{id}")
    class Id(val parent: Users = Users(), val id: Int){
        @Resource("recipes")
        class Recipes(val parent: Id) {
            @Resource("{recipeId}")
            class RecipeId(val parent: Recipes, val recipeId: Int)
        }

        @Resource("stock")
        class StockIngredients(val parent: Id){
            @Resource("{ingredientId}")
            class IngredientId(val parent: StockIngredients, val ingredientId: Int)
        }

        @Resource("shop")
        class ShopIngredients(val parent: Id){
            @Resource("{ingredientId}")
            class IngredientId(val parent: ShopIngredients, val ingredientId: Int)
        }
    }


    @Resource("{name}")
    class Name(val parent : Users = Users(), val name: String)
}

fun Route.userRoutesWithoutAuth(dao: DAOFacade) {

    /**
     * Handle the POST request for creating a new user.
     */
    post<Users> {
        val formParameters = call.receiveParameters()
        val username = formParameters["username"].toString()
        val email = formParameters["email"].toString()
        val password = formParameters["password"].toString()

        val newUser = dao.addUser(username, email, password)
        call.respond(HttpStatusCode.Created, "$newUser")

    }


    /**
     * Handle the POST request for logging in a user.
     */
    post<Login> {
        val formParameters = call.receiveParameters()
        val username = formParameters["username"].toString()
        val password = formParameters["password"].toString()

        val user = dao.getUserByUsername(username)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            if (user.password == password) {
                call.respond(HttpStatusCode.OK, "$user")
            } else {
                call.respond(HttpStatusCode.Unauthorized, "Incorrect password.")
            }
        }
    }

}

fun Route.usersRoutes(dao: DAOFacade){
    /**
     * Handles the GET request for retrieving all users.
     */
    get<Users> {
        val users = dao.allUsers()
        call.respond(users)
    }


    /**
     * Handle the PUT request for updating a user.
     */
    put<Users.Id> {
        val id = call.parameters["id"]?.toIntOrNull()
        if (id == null) {
            call.respond(HttpStatusCode.BadRequest, "Invalid user id.")
            return@put
        }
        val formParameters = call.receive<User>()
        dao.editUser(id, formParameters.username, formParameters.email, formParameters.password, formParameters.recipes, formParameters.stockIngredients, formParameters.shopIngredients)

        call.respond(HttpStatusCode.OK, "User updated successfully.")
    }


    /**
     * Handle the DELETE request for deleting a user.
     */
    delete<Users.Id> { userId ->

        val success = dao.deleteUser(userId.id)
        if (success){
            call.respond(HttpStatusCode.OK, "User deleted successfully.")
        } else {
            call.respond(HttpStatusCode.InternalServerError, "Failed to delete user.")
        }
    }


    /**
     * Handle the GET request for retrieving a user by id.
     */
    get<Users.Id> { id ->
        val user = dao.getUserById(id.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            call.respond(user)
        }
    }

    /**
     * Handle the GET request for retrieving recipes of a user.
     */

    get<Users.Id.Recipes> {
        val user = dao.getUserById(it.parent.id)
        if (user == null){
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            val recipes = dao.getRecipesByUserId(user.userId)
            call.respond(recipes)
        }
    }

    /**
     * Handle the GET request for retrieving stock ingredients of a user.
     */

    get<Users.Id.StockIngredients> {
        val user = dao.getUserById(it.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            val stockIngredients = dao.getStockIngredientsByUserId(user.userId)
            call.respond(stockIngredients)
        }
    }

    /**
     * Handle the GET request for retrieving shop ingredients of a user.
     */

    get<Users.Id.ShopIngredients> {
        val user = dao.getUserById(it.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            val shopIngredients = dao.getShopIngredientsByUserId(user.userId)
            call.respond(shopIngredients)
        }
    }

    /**
     * Handle the POST request for creating a new recipe for a user.
     */

    post<Users.Id.Recipes> {
        val recipe = call.receive<Recipe>()
        val user = dao.getUserById(it.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            val recipeExists = dao.getRecipeById(recipe.recipeId)
            if (recipeExists == null) {
                call.respond(HttpStatusCode.NotFound, "Recipe not found.")
            } else {
                dao.addRecipeToUser(user.userId, recipe.recipeId)
                call.respond(HttpStatusCode.OK, "Recipe added to user successfully.")
            }
        }
    }

    /**
     * Handle the POST request for adding a stock ingredient to a user.
     */

    post<Users.Id.StockIngredients> {
        val ingredient = call.receive<IngredientContainer>()
        val user = dao.getUserById(it.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            val ingredientExists = dao.getIngredientById(ingredient.ingredient.ingredientId)
            if (ingredientExists == null) {
                call.respond(HttpStatusCode.NotFound, "Ingredient not found.")
            } else {
                dao.addIngredientToUserStock(user.userId, ingredient)
                call.respond(HttpStatusCode.OK, "Ingredient added to user successfully.")
            }
        }
    }

    /**
     * Handle the POST request for adding a shop ingredient to a user.
     */

    post<Users.Id.ShopIngredients> {
        val ingredient = call.receive<IngredientContainer>()
        val user = dao.getUserById(it.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            val ingredientExists = dao.getIngredientById(ingredient.ingredient.ingredientId)
            if (ingredientExists == null) {
                call.respond(HttpStatusCode.NotFound, "Ingredient not found.")
            } else {
                dao.addIngredientToUserShop(user.userId, ingredient)
                call.respond(HttpStatusCode.OK, "Ingredient added to user successfully.")
            }
        }
    }

    /**
     * Handle the DELETE request for deleting a recipe from a user.
     */

    delete<Users.Id.Recipes.RecipeId>{
        val user = dao.getUserById(it.parent.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
           dao.deleteRecipeFromUser(user.userId, it.recipeId)
           call.respond(HttpStatusCode.OK, "Recipe deleted from user successfully.")
        }
    }

    /**
     * Handle the DELETE request for deleting a stock ingredient from a user.
     */

    delete<Users.Id.StockIngredients.IngredientId>{
        val user = dao.getUserById(it.parent.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            dao.deleteIngredientFromUserStock(user.userId, it.ingredientId)
            call.respond(HttpStatusCode.OK, "Ingredient deleted from user successfully.")
        }
    }

    /**
     * Handle the DELETE request for deleting a shop ingredient from a user.
     */

    delete<Users.Id.ShopIngredients.IngredientId> {
        val user = dao.getUserById(it.parent.parent.id)
        if (user == null) {
            call.respond(HttpStatusCode.NotFound, "User not found.")
        } else {
            dao.deleteIngredientFromUserShop(user.userId, it.ingredientId)
            call.respond(HttpStatusCode.OK, "Ingredient deleted from user successfully.")
        }
    }
}


