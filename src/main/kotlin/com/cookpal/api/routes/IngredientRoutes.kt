package com.cookpal.api.routes

import com.cookpal.api.dao.DAOFacade
import com.cookpal.api.models.Category
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.resources.post
import io.ktor.server.resources.put
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import java.io.File
import java.io.InputStream
import java.io.OutputStream

@Resource("/ingredients")
class Ingredients() {
    @Resource("{id}")
    class Id(val parent: Ingredients = Ingredients(), val id: Long)
    @Resource("{name}")
    class Name(val parent: Ingredients = Ingredients(), val name: String)
}


fun Route.ingredientsRoutes(dao: DAOFacade, uploadDir: File) {


    /**
     * Handle the GET request for all ingredients.
     */
    get<Ingredients> {
        val ingredients = dao.allIngredients()
        call.respond(ingredients)
    }


    /**
     * Handle the GET request for a specific ingredient.
     */
    get<Ingredients.Id> { ingredientId ->
        val ingredient = dao.getIngredientById(ingredientId.id.toInt())
        if (ingredient != null) {
            call.respond(ingredient)
        } else {
            call.respondText("El ingrediente con id ${ingredientId.id} no existe.", status = HttpStatusCode.NotFound)
        }
    }


    /**
     * Handle the GET request for a specific ingredient.
     */
    get<Ingredients.Name> { ingredientName ->
        val ingredient = dao.getIngredientByName(ingredientName.name)
        if (ingredient != null) {
            call.respond(ingredient)
        } else {
            call.respondText(
                "El ingrediente con nombre ${ingredientName.name} no existe.",
            )
        }
    }


    /**
     * Handle the POST request for creating a new ingredient.
     */
    post<Ingredients> { partData ->
        val bodyData = call.receiveMultipart();
        var ingredientName = ""
        var ingredientCategory: Category = Category.OTROS
        var ingredientImage: File? = null
        bodyData.forEachPart { partData ->
            when (partData) {
                is PartData.FormItem -> {
                    if (partData.name == "name") {
                        ingredientName = partData.value
                    } else if (partData.name == "category") {
                        ingredientCategory = Category.valueOf(partData.value)
                    }
                }

                is PartData.FileItem -> {

                    val ext = File(partData.originalFileName).extension
                    val file = File(
                        uploadDir,
                        "upload-${System.currentTimeMillis()}-${ingredientName.hashCode()}-${ingredientCategory.hashCode()}.$ext"
                    )

                    partData.streamProvider()
                        .use { its -> file.outputStream().buffered().use { its.copyToSuspend(it) } }
                    ingredientImage = file

                }

                else -> {}
            }

        }
        if (ingredientImage != null) {
            dao.addNewIngredient(ingredientName, ingredientCategory, ingredientImage?.name)
        } else {
            dao.addNewIngredient(ingredientName, ingredientCategory, null)
        }

        call.respond("Se ha agregado correctamente el ingrediente.")
    }


    /**
     * Handle the PUT request for updating an ingredient.
     */
    put<Ingredients.Id> { ingredientId ->
        val ingredient = dao.getIngredientById(ingredientId.id.toInt())
        if (ingredient == null) {
            call.respondText(
                "El ingrediente con id ${ingredientId.id} no existe.",
                status = HttpStatusCode.NotFound
            )
            return@put
        }

        val bodyData = call.receiveMultipart()
        var ingredientName = ""
        var ingredientCategory: Category = Category.OTROS
        var ingredientImage: File? = null
        var hasImage = false
        bodyData.forEachPart { partData ->
            when (partData) {
                is PartData.FormItem -> {
                    when (partData.name) {
                        "name" -> ingredientName = partData.value
                        "category" -> ingredientCategory = Category.valueOf(partData.value)
                    }
                }

                is PartData.FileItem -> {
                    val ext = File(partData.originalFileName.toString()).extension
                    val file = File(
                        uploadDir,
                        "upload-${System.currentTimeMillis()}-${ingredientName.hashCode()}-${ingredientCategory.hashCode()}.$ext"
                    )
                    partData.streamProvider().use { input ->
                        file.outputStream().buffered().use { output ->
                            input.copyToSuspend(output)
                        }
                    }
                    ingredientImage = file
                    hasImage = true
                }

                else -> {}
            }
        }

        ingredientName = if (ingredientName.isEmpty()) ingredient.name else ingredientName
        ingredientCategory = if (ingredientCategory != ingredient.category) ingredient.category else ingredientCategory
        val image = if (hasImage) ingredientImage!!.name else ingredient.img

        val success = dao.editIngredient(ingredientId.id.toInt(), ingredientName, ingredientCategory, image)
        if (success) {
            call.respond("El ingrediente con id ${ingredientId.id} se ha actualizado correctamente.")
        } else {
            call.respondText(
                "Error al actualizar el ingrediente con id ${ingredientId.id}.",
                status = HttpStatusCode.InternalServerError
            )
        }
    }


    /**
     * Handle the DELETE request for deleting an ingredient.
     */
    delete<Ingredients.Id> { ingredientId ->
        val success = dao.deleteIngredient(ingredientId.id.toInt())
        if (success) {
            call.respond("El ingrediente con id ${ingredientId.id} se ha eliminado correctamente.")
        } else {
            call.respondText(
                "El ingrediente con id ${ingredientId.id} no existe.",
                status = HttpStatusCode.NotFound
            )
        }
    }



}

suspend fun InputStream.copyToSuspend(
    out: OutputStream,
    bufferSize: Int = DEFAULT_BUFFER_SIZE,
    yieldSize: Int = 4 * 1024 * 1024,
    dispatcher: CoroutineDispatcher = Dispatchers.IO
): Long {
    return withContext(dispatcher) {
        val buffer = ByteArray(bufferSize)
        var bytesCopied = 0L
        var bytesAfterYield = 0L
        while (true) {
            val bytes = read(buffer).takeIf { it >= 0 } ?: break
            out.write(buffer, 0, bytes)
            if (bytesAfterYield >= yieldSize) {
                yield()
                bytesAfterYield %= yieldSize
            }
            bytesCopied += bytes
            bytesAfterYield += bytes
        }
        return@withContext bytesCopied
    }

}
