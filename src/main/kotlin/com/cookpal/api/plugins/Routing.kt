package com.cookpal.api.plugins

import com.cookpal.api.dao.DAOFacade
import com.cookpal.api.routes.ingredientsRoutes
import com.cookpal.api.routes.recipesRoutes
import com.cookpal.api.routes.userRoutesWithoutAuth
import com.cookpal.api.routes.usersRoutes
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.resources.*
import io.ktor.server.routing.*
import java.io.File

fun Application.configureRouting(dao: DAOFacade, uploadDir: File) {
    install(Resources)
    routing {
        authenticate("auth-basic") {
            ingredientsRoutes(dao, uploadDir)
            recipesRoutes(dao)
            usersRoutes(dao)
        }
        userRoutesWithoutAuth(dao)
    }
}
