package com.cookpal.api.plugins

import com.cookpal.api.dao.DAOFacade
import io.ktor.server.application.*
import io.ktor.server.auth.*

fun Application.configureSecurity(dao: DAOFacade) {
    install(Authentication) {
        basic("auth-basic") {
            realm = "Access to the '/' path"
            validate { credentials ->
                val user = dao.getUserByUsername(credentials.name)
                if (user != null && user.password == credentials.password) {
                    UserIdPrincipal(credentials.name)
                } else {
                    null
                }
            }
        }
    }
}