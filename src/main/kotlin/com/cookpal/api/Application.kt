package com.cookpal.api

import com.cookpal.api.dao.DAOFacade
import com.cookpal.api.dao.DAOFacadeImpl
import com.cookpal.api.dao.DatabaseFactory
import com.cookpal.api.plugins.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File
import java.io.IOException
import java.util.*

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)
@Suppress("unused") // application.conf references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {

    val dao: DAOFacade = DAOFacadeImpl()

    val config = environment.config.config("cookpal")
    val uploadDirPath: String = config.property("upload.dir").getString()
    val uploadDir = File(uploadDirPath)
    if (!uploadDir.mkdirs() && !uploadDir.exists()) {
        throw IOException("Failed to create directory ${uploadDir.absolutePath}")
    }

    DatabaseFactory.init()
    configureHTTP()
    configureSerialization()
    configureSecurity(dao)
    configureRouting(dao, uploadDir)

}


