package com.cookpal.api.models

import kotlinx.serialization.Serializable

@Serializable
data class Ingredient(
    val ingredientId:  Int,
    val name: String,
    val category: Category,
    val img: String?
)