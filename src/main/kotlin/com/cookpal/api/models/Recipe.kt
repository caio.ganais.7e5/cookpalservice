package com.cookpal.api.models

import kotlinx.serialization.Serializable

@Serializable
data class Recipe(
    val recipeId: Int,
    val name: String,
    val dinnerCommensal: Int,
    val cookTime: Int,
    val ingredients: List<IngredientContainer>,
    val steps : List<String>
)