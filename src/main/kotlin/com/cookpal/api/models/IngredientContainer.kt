package com.cookpal.api.models

import kotlinx.serialization.Serializable

@Serializable
data class IngredientContainer(
    val ingredient: Ingredient,
    val measure: String,
    val quantity: Int
)