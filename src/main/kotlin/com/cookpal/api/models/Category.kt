package com.cookpal.api.models

import kotlinx.serialization.Serializable

@Serializable
enum class Category {
    CEREALES,
    LEGUMBRES,
    FRUTOS_SECOS,
    GRANOS,
    SEMILLAS,
    HARINAS,
    CARNES_Y_PESCADOS,
    MARISCOS,
    LACTEOS_Y_HUEVOS,
    QUESOS,
    YOGURES,
    CREMAS,
    ACEITES,
    VINAGRES,
    SAL,
    AZUCAR,
    MIEL,
    MERMELADA,
    SALSAS,
    ESPECIAS,
    HIERBAS,
    VERDURAS,
    HORTALIZAS,
    FRUTAS,
    BEBIDAS,
    CONDIMENTOS,
    ADITIVOS,
    OTROS
}