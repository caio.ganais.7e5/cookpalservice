package com.cookpal.api.models

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val userId: Int,
    val username: String,
    val password: String,
    val email: String,
    val recipes: List<Recipe>,
    val stockIngredients: List<IngredientContainer>,
    val shopIngredients: List<IngredientContainer>
)
